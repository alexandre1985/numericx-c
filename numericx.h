/** @file numericx.h
 * Numericx library header file.
 */
#ifndef _NUMERICX_H
#define _NUMERICX_H

#include <stdbool.h>


/* ||FUNCTIONS|| */

void
numericx_free(char* string);

int
numericx_translate(char* from, bool from_units_on_the_end, bool from_first_number_void, bool from_infinite_base, char* to, bool to_units_on_the_end, bool to_first_number_void, bool to_infinite_base, char* number, char** result_string);

#endif
