#!/bin/bash

current_branch=$(git rev-parse --abbrev-ref HEAD)
git checkout stable
make build-tests
for i in {1..6} ; do \mv decimal-to-earth_test-${i} decimal-to-earth_test-${i}_stable; \mv earth-to-decimal_test-${i} earth-to-decimal_test-${i}_stable; done
git checkout ${current_branch}
make build-tests

for i in {1..6}; do for n in {1..1000}; do [[ "$(decimal-to-earth_test-${i} $n)" == "$(decimal-to-earth_test-${i}_stable $n)" ]] || echo "[X] failed: decimal-to-earth_test-${i} $n" ; done; echo " * decimal-to-earth_test-${i} clean"; done
for i in {1..6}; do for n in {1..1000}; do num=$(decimal-to-earth_test-${i} ${n}); [[ "$(earth-to-decimal_test-${i} ${num})" == "$(earth-to-decimal_test-${i}_stable ${num})" ]] || echo "[X] failed: earth-to-decimal_test-${i} ${num}" ; done; echo " * earth-to-decimal_test-${i} clean"; done

echo

echo "----"
echo -e "----\n * void: decimal-to-earth_test-1 0 -> " $([[ "$(decimal-to-earth_test-1 0)" == "$(decimal-to-earth_test-1_stable 0)" ]] && echo clean || echo failed)

echo "----"
echo -e "----\n * out of domain: decimal-to-earth_test-1 abc -> " $([[ "$(decimal-to-earth_test-1 abc)" == "$(decimal-to-earth_test-1_stable abc)" ]] && echo clean || echo failed)

