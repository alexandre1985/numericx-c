CFLAGS := -Wall -Wextra -O2
DEP := cli.c numericx.o

INSTALL := install

prefix = /usr

.PHONY: all
all: decimal-to-earth earth-to-decimal decimal-to-sun sun-to-decimal decimal-to-lori decimal-to-nonal_inf decimal-to-nonal lori-to-decimal nonal_inf-to-decimal nonal-to-decimal uninfinity decimal-to-binary binary-to-decimal

.PHONY: lib-dirs
lib-dirs:
	@mkdir -p lib/{static,shared}

numericx.o: numericx.c numericx.h
	$(CC) $(CFLAGS) -c numericx.c

.PHONY: libnumericx.a libnumericx.so
libnumericx.a: numericx.o
	@$(MAKE) lib-dirs
	ar -rcs lib/static/$@ $^
	@echo Compiled successfully to ./lib/static/

numericx-pic.o: numericx.c numericx.h
	$(CC) $(CFLAGS) -fPIC -c numericx.c -o $@

libnumericx.so: numericx-pic.o
	@$(MAKE) lib-dirs
	$(CC) -shared $^ -o lib/shared/$@
	@echo Compiled successfully to ./lib/shared/

.PHONY: libs install-lib
libs: libnumericx.a libnumericx.so

install-lib: libnumericx.so
	$(INSTALL) -m 755 lib/shared/libnumericx.so $(prefix)/lib/
	$(INSTALL) -m 644 numericx.h $(prefix)/include/

.PHONY: doc
doc:
	rm -Rf doc/*
	doxygen

decimal-to-earth: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -o $@ $^

earth-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

decimal-to-sun: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"123456789\" -o $@ $^

sun-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

sun-to-earth: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789\" -DTO_NUMERICALS=\"12345\" -o $@ $^

earth-to-sun: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"123456789\" -o $@ $^

decimal-to-lori: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"123456789@\" -DTO_UNITS_ON_THE_END -o $@ $^

decimal-to-binary: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"01\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

lori-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789@\" -DFROM_UNITS_ON_THE_END -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

nonal-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

binary-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"01\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

decimal-to-nonal_inf: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"123456789\" -DTO_UNITS_ON_THE_END -DTO_INFINITE_BASE -o $@ $^

decimal-to-nonal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"123456789\" -DTO_UNITS_ON_THE_END -o $@ $^

nonal_inf-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789\" -DFROM_UNITS_ON_THE_END -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

nonal-to-decimal: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"123456789\" -DFROM_UNITS_ON_THE_END -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

uninfinity: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_UNITS_ON_THE_END -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"0123456789\" -DTO_UNITS_ON_THE_END -DTO_FIRST_NUMBER_VOID -o $@ $^

.PHONY: clean
clean:
	\rm -f *-to-* uninfinity *.o lib/static/*.a lib/shared/*.so

.PHONY: run
run: decimal-to-earth
	./decimal-to-earth 21

.PHONY: mem mem1 mem2 mem3 mem4
mem:
	echo "the memory make rules are: mem1 mem2 mem3 mem4"

mem1: decimal-to-earth
	valgrind --leak-check=full --show-leak-kinds=all -s decimal-to-earth 999

mem2: decimal-to-earth
	valgrind --leak-check=full --show-leak-kinds=all -s decimal-to-earth 000

mem3: decimal-to-earth
	valgrind --leak-check=full --show-leak-kinds=all -s decimal-to-earth abc

mem4: decimal-to-earth
	valgrind --leak-check=full --show-leak-kinds=all -s decimal-to-earth 2 3


decimal-to-earth_test-1: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -o $@ $^

earth-to-decimal_test-1: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -o $@ $^

decimal-to-earth_test-2: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -DFROM_UNITS_ON_THE_END -o $@ $^

earth-to-decimal_test-2: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -DTO_UNITS_ON_THE_END -o $@ $^

decimal-to-earth_test-3: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -DTO_UNITS_ON_THE_END -o $@ $^

earth-to-decimal_test-3: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -DFROM_UNITS_ON_THE_END -o $@ $^

decimal-to-earth_test-4: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -DFROM_UNITS_ON_THE_END -DTO_UNITS_ON_THE_END -o $@ $^

earth-to-decimal_test-4: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -DFROM_UNITS_ON_THE_END -DTO_UNITS_ON_THE_END -o $@ $^

decimal-to-earth_test-5: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -DTO_FIRST_NUMBER_VOID -o $@ $^

earth-to-decimal_test-5: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -DFROM_FIRST_NUMBER_VOID -o $@ $^

decimal-to-earth_test-6: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"0123456789\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DTO_NUMERICALS=\"12345\" -DTO_INFINITE_BASE -o $@ $^

earth-to-decimal_test-6: $(DEP)
	$(CC) $(CFLAGS) -DFROM_NUMERICALS=\"12345\" -DTO_NUMERICALS=\"0123456789\" -DTO_FIRST_NUMBER_VOID -DTO_INFINITE_BASE -DFROM_INFINITE_BASE -o $@ $^


.PHONY: build-tests
build-tests: decimal-to-earth_test-1 earth-to-decimal_test-1 decimal-to-earth_test-2 earth-to-decimal_test-2 decimal-to-earth_test-3 earth-to-decimal_test-3 decimal-to-earth_test-4 earth-to-decimal_test-4 decimal-to-earth_test-5 earth-to-decimal_test-5 decimal-to-earth_test-6 earth-to-decimal_test-6

.PHONY: test
test:
	tests/earth.sh
