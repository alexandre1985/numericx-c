# Numericx Readme

A program that converts a number (or text) from one numerical system into another different numerical system.

## C library

### Create libraries

You may use `make libs` to create a static and a shared numericx library. To install a shared library into your system use `sudo make prefix=/usr install-lib`.

### Functions

See [Numericx documentation website](https://danisanti.tilde.institute/docs/numericx-c/), for the list of functions and function arguments of the Numericx library.
Look at `Files > File List` in the documentation menubar website.

There are basically two main functions: the `numericx_translate()` and the `numericx_free()`. Remember to free your numericx result with `numericx_free(var);`!

### Code example

Look at the `cli.c` file for an example of how to call Numericx in your C program.

Here is a code snippet:

```
(...)

/* 'result' variable has to be NULL */
char* result = NULL;

/*
 * 'from', 'to' and 'number' are strings (malloced or literals, not arrays).
 * the rest of the variables, except 'result', are booleans
 *
 * Do the translation
 */
int status = numericx_translate(
		from_numericals, from_units_on_the_end, from_first_number_void, from_infinite_base,
		to_numericals, to_units_on_the_end, to_first_number_void, to_infinite_base,
		number, &result);

/* Test for translation failure */
switch( status )
{
	case EINVAL:
		fprintf(stderr, "error: numericx: %s. Resulting string argument value has to be NULL.\n",
				strerror(EINVAL));
		break;
	case EDOM:
		fprintf(stderr, "error: numericx: %s. Valid numerals are: \"%s\".\n",
				strerror(EDOM), from);
		break;
	case ERANGE:
		fprintf(stderr, "error: numericx: Unrepresentable void number.\n");
		break;
}

/* General error message with free and exit() */
if( !(status == true) )
{
	fprintf(stderr, "error: numericx: Incapable of translating.\n");
	numericx_free(result);

	exit(EXIT_FAILURE);
}

/* Print translated number ('result') in case of success */
printf("%s\n", result);

/* Free translated number, when no longer needed */
numericx_free(result);

(...)
```


## CLI

### Compiling

With this program, you can create an executable (or many) that will convert a number from one numerical system into another one. This is good if, for example, you want to use numerical system translation with your bash terminal.

For example, you may want to convert a hexadecimal number into a ternary number. Here is what you would do.

You first need to define the proprieties of the numerical systems. These proprieties are defined in the compilation process (as compilation flags).

For example, let's make the hexdecimal numerical system with numerals from `0-f`, with a infinite 0 and it start counting on 1. So, our hexadecimal system will need the following compilation flags:
```
-DFROM_NUMERICALS=\"0123456789abcdef\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE
```

if you want the units place of this numerical system to be on the right side, add `-DFROM_UNITS_ON_THE_END`.

Now, for the ternary system let's make with numerals from `1-3`, 1 is not infinite and it start counting on 1. Use this flags:
```
-DTO_NUMERICALS=\"123\"
```

if you want the units place of this numerical system to be on the right side, use `-DTO_UNITS_ON_THE_END`.

Now, to create the executable to translate a number for these two numerical system, join all of this together and compile using this:
```
$ cc -DFROM_NUMERICALS=\"0123456789abcdef\" -DFROM_FIRST_NUMBER_VOID -DFROM_INFINITE_BASE -DFROM_UNITS_ON_THE_END -DTO_NUMERICALS=\"123\" -DTO_UNITS_ON_THE_END numericx.c cli.c -o hex-to-ternary
```

This creates the `hex-to-ternary` executable with can be used to convert numbers from hexadecimal to ternary!

### Pre-defined executable compilation

There are many pre-defined compilation of executables on the Makefile. Take a look at the Makefile to know them.
Or you can run `make` to get all of the pre-defined executables. `make clean` to delete them.

### Compilation flags meanings

There are two categories for the compilation flags: the `FROM` category and the `TO` category.

`FROM` flags are the numerical system definitions of the argument number.

`TO` flags are the numerical system definitions of the number result.

Below, are the meanings of the `FROM` flags. The `TO` flags are the same as the `FROM` flags, with a switch of the `FROM_` to a `TO_`.

|Flag|Meaning|
|---|---|
|`FROM_NUMERICALS`|Define the numerical system numerals of the 'from'|
|`FROM_UNITS_ON_THE_END`|Defines the units case to be on the end (on the right) for the 'from'|
|`FROM_FIRST_NUMBER_VOID`|Defines the first number as a "not counting" for the 'from'|
|`FROM_INFINITE_BASE`|Defines the first number to be infinite, for the 'from' (for example, if the first numeral is 0, then 0 == 00 == 000 == 0000 ... . Or if the first numeral is 1, then 1 == 11 == 111 ...)|

Remember: you use put a `-D` before these flags names as an compiler argument. For example, `-DFROM_NUMERICALS=...`.


## License

MIT


## Author

Daniel A. C. Santos
